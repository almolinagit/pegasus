//////////////////////////////////
// cc-utils.ts
// DCL Converence Center Utlities
// (c) 2019 Decentraland Conference Center LLC

import {spawnBoxX} from 'SpawnerFunctions'
export {spawnGround, spawnPath, spawnCrossPath}

const LW = 16
const PW = 6
const SW = (16-PW)/2
const minY = 0.02

///////////////////
// spawnGround
// Spawns a plot of ground with material
// Coordinates x,z are south west of the plot, based on Entity as origin in scene
// Sized by dx, dy, dz.  
// dy can be set at zero, but if dY is < minY, then it is set to minY
// All units in meters.
// Material m applied
function spawnGround(parent:Entity, originX:number, originZ:number, dX:number, dY:number, dZ:number, mtl:Material)
{
    if (dY < minY) {
        dY = minY
    }
    let ground = spawnBoxX(originX+dX/2, dY/2, originZ+dZ/2, 0, 0, 0, dX, dY, dZ)
    ground.addComponent(mtl)
    ground.setParent(parent)
}

///////////////////
// spawnPath
// Spawns a parcel-wide path (PW m wide, with two SW m side strips)
// parent: its location is the origin (SW corner) of the path
// bEast: if true, path runs to the east, if false, runs to the north.
// parcels: length of the path, in parcels
// centerMaterial is applied to the path
// sidesMateria is applied to the two side strips
function spawnPath(parent:Entity, bEast:boolean, parcels:number, dYcenter:number, dYsides:number, centerMaterial: Material, sidesMaterial: Material){
    if (dYcenter < minY) {
        dYcenter = minY
    }   
    if (dYsides < minY) {
        dYsides = minY
    }   
    let path = spawnBoxX((bEast?parcels*LW/2:LW/2), dYcenter/2, (bEast?LW/2:parcels*LW/2), 0, 0, 0, (bEast?parcels*LW:PW), dYcenter, (bEast?PW:parcels*LW))
    path.addComponent(centerMaterial)
    path.setParent(parent)
    let leftSide= spawnBoxX((bEast?parcels*LW/2:SW/2), dYsides/2, (bEast?SW/2:parcels*LW/2),  0, 0, 0, (bEast?parcels*LW:SW), dYsides, (bEast?SW:parcels*LW))
    leftSide.addComponent(sidesMaterial)
    leftSide.setParent(parent)
    let rightSide= spawnBoxX((bEast?parcels*8:(PW+SW+SW/2)), dYsides/2, (bEast?(PW+SW+SW/2):parcels*8),  0, 0, 0, (bEast?parcels*16:SW), dYsides, (bEast?SW:parcels*16))
    rightSide.addComponent(sidesMaterial)
    rightSide.setParent(parent)
}

function spawnCrossPath (parent:Entity, bWest: boolean, bEast: boolean, bSouth: boolean, bNorth: boolean, dYcenter:number, dYsides:number, centerMaterial: Material, sidesMaterial: Material){
    if (dYcenter < minY) {
        dYcenter = minY
    }   
    if (dYsides < minY) {
        dYsides = minY
    } 
    if (bWest) { //W
        if (bEast) {//W,E 
            spawnGround(parent, 0,SW,LW,dYcenter,PW,centerMaterial) // WE path strip
            if (bNorth){//W,E,N
                spawnGround(parent, SW,SW+PW,PW,dYcenter,SW,centerMaterial) // North ray path strip
                if (bSouth) { // N, S, E, W  ***** Cross
                    spawnGround(parent,SW, 0, PW, dYcenter,SW, centerMaterial) //South ray Path strip  
                    spawnGround(parent, SW+PW, SW+PW, SW, dYsides, SW, sidesMaterial) // NE Corner
                    spawnGround(parent, 0, SW+PW, SW, dYsides, SW, sidesMaterial) // NW corner
                    spawnGround(parent, 0, 0, SW, dYsides, SW, sidesMaterial) // SW corner
                    spawnGround(parent, SW+PW, 0, SW, dYsides, SW, sidesMaterial) // SE corner
                }
                else { // N, !S, E, W ***** Upside Down T
                    spawnGround(parent, 0, 0, LW, dYsides, SW, sidesMaterial)  //EW strip at the south
                    spawnGround(parent, 0, SW+PW, SW, dYsides, SW, sidesMaterial) // NW corner
                    spawnGround(parent, SW+PW, SW+PW, SW, dYsides, SW, sidesMaterial) // NE corner
                }
            }
            else {//W, E, !N
                if (bSouth){ //W, E, !N, S ***** Upright T
                    spawnGround(parent, SW, 0, PW, dYcenter,SW, centerMaterial) //South ray Path strip  
                    spawnGround(parent, 0, SW+PW, LW, dYsides, SW, sidesMaterial) // full EW strip on North
                    spawnGround(parent, SW+PW, 0, SW, dYsides, SW, sidesMaterial) // SE corner
                    spawnGround(parent, 0, 0, SW, dYsides, SW, sidesMaterial) // SW Corner
                }
                else { //W, E, !N, !S ***** EW path, equiv to spawnPaty true
                    spawnGround(parent, 0, SW+PW, LW, dYsides, SW, sidesMaterial) // full EW strip on North
                    spawnGround(parent, 0, 0, LW, dYsides, SW, sidesMaterial)  //EW strip at the south
                }
            }
        }
        else { // W, !E
            spawnGround(parent,SW+PW,0,SW,dYsides, LW,sidesMaterial) //NS strip on the east, must be T on its right, or NW or SW turn
            if (bNorth) { //W, !E, N
                if (bSouth) { // W,!E, N,S,  ***** T on its Right
                    spawnGround(parent, SW,0,PW,dYcenter,LW,centerMaterial) //NS path
                    spawnGround(parent, 0, SW, SW, dYcenter,PW, centerMaterial) // W ray
                    spawnGround(parent, 0, SW+PW, SW, dYsides, SW, sidesMaterial) // NW corner
                    spawnGround(parent, 0, 0, SW, dYsides, SW, sidesMaterial) // SW corner
                }
                else { // W, !E N, !S ***** NW Turn
                    spawnGround(parent, 0, SW, SW+PW,dYcenter,PW,centerMaterial) // West and Center path
                    spawnGround(parent, SW,SW+PW,PW,dYcenter,SW,centerMaterial) // North ray Path
                    spawnGround(parent,0, 0, LW, dYsides, SW, sidesMaterial) // paint EW strip on S
                    spawnGround(parent, 0, SW+PW, SW, dYsides, SW, sidesMaterial) // paint NW corner
                }
            }
            else { // W, !E, !N
                if (bSouth) { // W, !E, !N, S  ***** SW Turn
                    spawnGround(parent, 0, SW, SW+PW,dYcenter,PW,centerMaterial) // West and Center path
                    spawnGround(parent,SW, 0, PW, dYcenter,SW, centerMaterial) //South ray Path 
                    spawnGround(parent, 0, SW+PW, PW+SW, dYsides, SW, sidesMaterial) //  paint W-C strip @N
                    spawnGround(parent, 0, 0, SW, dYsides, SW, sidesMaterial) //  paint SW corner
                }
                else { // W, !E, !N, !S  ***** W Only
                    spawnGround(parent, 0, SW, SW+PW,dYcenter,PW,centerMaterial) // West and Center path
                    spawnGround(parent, 0, SW+PW, PW+SW, dYsides, SW, sidesMaterial) //  paint W-C strip @N
                    spawnGround(parent, 0, 0, PW+SW, dYsides, SW, sidesMaterial) //  paint W-C strip @S
                }

            }
        }  
    }
    else { // !W
        spawnGround(parent,0,0,SW,dYsides, LW,sidesMaterial) //NS strip on the west
        if (bNorth) { //!W, N
            if (bSouth){ //!W, N, S
                spawnGround(parent, SW,0,PW,dYcenter,LW,centerMaterial) //NS strip path
                if (bEast){// !W, N, S, E ***** T on its Left
                    spawnGround(parent, SW+PW, SW, SW, dYcenter,PW, centerMaterial) // E ray path
                    spawnGround(parent, SW+PW, SW+PW, SW, dYsides, SW, sidesMaterial) // NE Corner
                    spawnGround(parent, SW+PW, 0, SW, dYsides, SW, sidesMaterial) // SE corner
                } 
                else { //!W, N, S, !E ***** NS path,
                    spawnGround(parent, SW, 0, PW,dYcenter,LW,centerMaterial) // Center and north ray of path
                    spawnGround(parent,SW+PW,0,SW,dYsides, LW,sidesMaterial) //NS strip on the E
                }
            }
            else {// !W, N !S
                if (bEast) { // !W, S!, N, E ***** NE Turn
                    spawnGround(parent, SW, SW, SW+PW,dYcenter,PW,centerMaterial) // Center & E path
                    spawnGround(parent, SW,SW+PW,PW,dYcenter,SW,centerMaterial) // North ray Path
                    spawnGround(parent, SW, 0, PW+SW, dYsides, SW, sidesMaterial) //Center and E at the South
                    spawnGround(parent, SW+PW, SW+PW, SW, dYsides, SW, sidesMaterial) // NE Corner
                }
                else { // !W, N, !S, !E ***** N only
                    spawnGround(parent, SW, SW, PW,dYcenter,SW+PW,centerMaterial) // Center and north ray of path
                    spawnGround(parent,SW+PW,0,SW,dYsides, LW,sidesMaterial) //NS strip on the E
                    spawnGround(parent, SW, 0, PW, dYsides, SW, sidesMaterial) // S spot
                }
            }
        }
        else {// !W, !N
            if (bSouth){ // !W, !N , S
                if (bEast) { //!W, !N, S, E ***** SE Turn
                    //spawnGround(parent, SW,SW+PW,PW,dYcenter,SW,centerMaterial) // North ray Path
                    spawnGround(parent, SW, SW, SW+PW,dYcenter,PW,centerMaterial) // Center & E path
                    spawnGround(parent, SW, 0, PW, dYcenter,SW, centerMaterial) //South ray Path 
                    spawnGround(parent, SW, SW+PW, PW+SW, dYsides, SW, sidesMaterial) // Center and E at the N
                    spawnGround(parent, SW+PW, 0, SW, dYsides, SW, sidesMaterial) // SE corner
                }
                else { //!W, !N S, !E ***** South only
                    spawnGround(parent, SW, 0, PW,dYcenter,PW+SW,centerMaterial) // Center and south ray of path

                    spawnGround(parent, SW, SW+PW, PW, dYsides, SW, sidesMaterial) // N spot
                    spawnGround(parent,SW+PW,0,SW,dYsides, LW,sidesMaterial) //NS strip on the E
                    spawnGround(parent, SW, SW+PW, PW, dYsides, SW, sidesMaterial) // N spot
                }
            }
            else {
                if (bEast) { // !W, !N  !S E ***** E Only
                    spawnGround(parent, SW, SW, SW+PW,dYcenter,PW,centerMaterial) // Center & E path
                    spawnGround(parent, SW, SW+PW, PW+SW, dYsides, SW, sidesMaterial) // Center and E at the N
                    spawnGround(parent, SW, 0, PW+SW, dYsides, SW, sidesMaterial) // Center and E at the S
                    
                }
                else { // !W !N !S !E ***** Center
                    spawnGround(parent, SW, SW, PW,dYcenter,PW,centerMaterial) // Center square of path
                    spawnGround(parent,SW+PW,0,SW,dYsides, LW,sidesMaterial) //NS strip on the E
                    spawnGround(parent, SW, SW+PW, PW, dYsides, SW, sidesMaterial) // N spot
                    spawnGround(parent, SW, 0, PW, dYsides, SW, sidesMaterial) // S spot
                }

            }
        }
    }
}


