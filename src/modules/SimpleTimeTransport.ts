//////////////////////////////////////////
// Simple Time Transport
// (c) 2019 Carl Fravel
//
// Features
// * Handles time cursor (now) and transport state changes
// * Receives and holds Time Markers from your code
// * Calls your EventManagers with TransportStateChange and TransportMarkerHit events
//
// Instructions for use:
// 1. Create an instance of SimpleMediaTransport in your 
// 2. Create one or more EventManagers for TransportStateChange events
// 3. Set up one or more EventManagers in your scene to receive TransportStateChange and TransportMarkerHit events
// 3. Call addTimeMarker method to register the times at which you want your EventManager to receive an event, and the index number you want to appear in that event
// 4. If you want to have multiple media streams playing, your code can have multiple EventManagers, with each one adding their own TimeMarkers
// 5. Call play, pause, stop, or playFrom or setTime methods, and handle the TransportStateChanged events that come back to you
// 6  Handle the events this sends to your EventManager(s) to take appropriate actions.
// 7. Read the now property to get the current time if needed

export { SimpleTimeTransport, TransportStateChange, TransportMarkerHit, TRANSPORT_TIMESET, TRANSPORT_STOP, TRANSPORT_PLAY, TRANSPORT_PAUSE}

const TRANSPORT_TIMESET: number = 0
const TRANSPORT_STOP: number = 1
const TRANSPORT_PLAY: number = 2
const TRANSPORT_PAUSE: number = 3

@EventConstructor()
class TransportStateChange {
  constructor(public time: number, public state: number) {}
}

@EventConstructor()
class TransportMarkerHit {
  constructor(public time: number, public index: number) {}
}

class SimpleTimeTransport  implements ISystem {
  now: number = 0 // current time cursor, in seconds
  playing: boolean = false
  markerTimes: number [] = []
  markerIndices: number[] = []
  markerListeners: EventManager[] = []
  numTimeMarkers: number = 0
  transportListeners: EventManager[] = []
  numTransportListeners = 0

  addTransportListener(manager:EventManager) {
    this.transportListeners[this.numTransportListeners] = manager
    this.numTransportListeners++
  }

  addTimeMarker(time: number, index: number, listener: EventManager) {
    this.markerTimes[this.numTimeMarkers] = time
    this.markerIndices[this.numTimeMarkers] = index
    this.markerListeners[this.numTimeMarkers] = listener
    this.numTimeMarkers++
  }

  reset () { // resets the entire transport to initial conditions - no listeners, no markers, not playing, now=0
    this.playing = false
    this.now = 0
    this.markerTimes = []
    this.markerIndices = []
    this.markerListeners = []
    this.numTimeMarkers = 0
    this.transportListeners = []
    this.numTransportListeners = 0
  }

  update(dt: number) {
    if (!this.playing) {
      return
    }
    let previousNow = this.now
    this.now += dt
    if (this.numTimeMarkers == 0) {
      return
    }
    // iterate through time markers to see which ones have just been reached or passsed (their time is > previous Time and <= now)
    for (let index in this.markerTimes ) {
      let markerTime:number = this.markerTimes[index]
      if ((markerTime > previousNow)&&(markerTime <= this.now)){
        this.markerListeners[index].fireEvent(new TransportMarkerHit(this.now, this.markerIndices[index]))
      }
    }
  }

  play () {
    if (!this.playing) {
      this.playing = true
      this.issueStateChange(TRANSPORT_PLAY)
    }
  }

  pause () {
    if (this.playing) {
      this.playing = false
      this.issueStateChange(TRANSPORT_PAUSE)
    }
  }

  stop () {
    if (this.playing){
      this.playing = false
      this.now = 0
      this.issueStateChange(TRANSPORT_STOP)
    }
  }

  setTime (t: number) {
    this.now = t
    this.issueStateChange(TRANSPORT_TIMESET)
  }

  issueStateChange (newState: number) {
    for (let listener of this.transportListeners ) {
      listener.fireEvent(new TransportStateChange(this.now, newState))
    }
  }
}

  