    
import {spawnGltfX, spawnEntity, spawnBoxX, spawnPlaneX, spawnTextX} from './modules/SpawnerFunctions'
import {BuilderHUD} from './modules/BuilderHUD'
import { GroundCover } from './modules/GroundCover'
import utils from "../node_modules/decentraland-ecs-utils/index"

const scene = new Entity()
const transform = new Transform({
  position: new Vector3(0, 0, 0),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
scene.addComponentOrReplace(transform)
engine.addEntity(scene)

const grassyFineTexture = new Texture("materials/Tileable-Textures/grassy-512-1-0.png")
const grassyFineMaterial = new Material()
grassyFineMaterial.albedoTexture = grassyFineTexture

const ground = new GroundCover (0,0,16,0.01,16,grassyFineMaterial,false)
ground.setParent(scene)

/*
const floorBaseGrass_01 = new Entity()
floorBaseGrass_01.setParent(scene)
const gltfShape = new GLTFShape('models/FloorBaseGrass_01/FloorBaseGrass_01.glb')
floorBaseGrass_01.addComponentOrReplace(gltfShape)
const transform_2 = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_01.addComponentOrReplace(transform_2)
engine.addEntity(floorBaseGrass_01)
*/

const fencePicketWhiteDoor_01 = new Entity()
fencePicketWhiteDoor_01.setParent(scene)
const gltfShape_2 = new GLTFShape('models/FencePicketWhiteDoor_01/FencePicketWhiteDoor_01.glb')
fencePicketWhiteDoor_01.addComponentOrReplace(gltfShape_2)
const transform_3 = new Transform({
  position: new Vector3(15.5, 0, 2.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01.addComponentOrReplace(transform_3)
engine.addEntity(fencePicketWhiteDoor_01)

const fencePicketWhiteDoor_01_2 = new Entity()
fencePicketWhiteDoor_01_2.setParent(scene)
fencePicketWhiteDoor_01_2.addComponentOrReplace(gltfShape_2)
const transform_4 = new Transform({
  position: new Vector3(15.5, 0, 5.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_2.addComponentOrReplace(transform_4)
engine.addEntity(fencePicketWhiteDoor_01_2)

const fencePicketWhiteDoor_01_3 = new Entity()
fencePicketWhiteDoor_01_3.setParent(scene)
fencePicketWhiteDoor_01_3.addComponentOrReplace(gltfShape_2)
const transform_5 = new Transform({
  position: new Vector3(15.5, 0, 4),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_3.addComponentOrReplace(transform_5)
engine.addEntity(fencePicketWhiteDoor_01_3)

const fencePicketWhiteDoor_01_4 = new Entity()
fencePicketWhiteDoor_01_4.setParent(scene)
fencePicketWhiteDoor_01_4.addComponentOrReplace(gltfShape_2)
const transform_6 = new Transform({
  position: new Vector3(15.5, 0, 2.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_4.addComponentOrReplace(transform_6)
engine.addEntity(fencePicketWhiteDoor_01_4)

const fencePicketWhiteDoor_01_5 = new Entity()
fencePicketWhiteDoor_01_5.setParent(scene)
fencePicketWhiteDoor_01_5.addComponentOrReplace(gltfShape_2)
const transform_7 = new Transform({
  position: new Vector3(15.5, 0, 2.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_5.addComponentOrReplace(transform_7)
engine.addEntity(fencePicketWhiteDoor_01_5)

const fencePicketWhiteDoor_01_6 = new Entity()
fencePicketWhiteDoor_01_6.setParent(scene)
fencePicketWhiteDoor_01_6.addComponentOrReplace(gltfShape_2)
const transform_8 = new Transform({
  position: new Vector3(15.5, 0, 8.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_6.addComponentOrReplace(transform_8)
engine.addEntity(fencePicketWhiteDoor_01_6)

const fencePicketWhiteDoor_01_7 = new Entity()
fencePicketWhiteDoor_01_7.setParent(scene)
fencePicketWhiteDoor_01_7.addComponentOrReplace(gltfShape_2)
const transform_9 = new Transform({
  position: new Vector3(15.5, 0, 8.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_7.addComponentOrReplace(transform_9)
engine.addEntity(fencePicketWhiteDoor_01_7)

const fencePicketWhiteDoor_01_8 = new Entity()
fencePicketWhiteDoor_01_8.setParent(scene)
fencePicketWhiteDoor_01_8.addComponentOrReplace(gltfShape_2)
const transform_10 = new Transform({
  position: new Vector3(15.5, 0, 8.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_8.addComponentOrReplace(transform_10)
engine.addEntity(fencePicketWhiteDoor_01_8)

const fencePicketWhiteDoor_01_9 = new Entity()
fencePicketWhiteDoor_01_9.setParent(scene)
fencePicketWhiteDoor_01_9.addComponentOrReplace(gltfShape_2)
const transform_11 = new Transform({
  position: new Vector3(15.5, 0, 10),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_9.addComponentOrReplace(transform_11)
engine.addEntity(fencePicketWhiteDoor_01_9)

const fencePicketWhiteDoor_01_10 = new Entity()
fencePicketWhiteDoor_01_10.setParent(scene)
fencePicketWhiteDoor_01_10.addComponentOrReplace(gltfShape_2)
const transform_12 = new Transform({
  position: new Vector3(15.5, 0, 11.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_10.addComponentOrReplace(transform_12)
engine.addEntity(fencePicketWhiteDoor_01_10)

const fencePicketWhiteDoor_01_11 = new Entity()
fencePicketWhiteDoor_01_11.setParent(scene)
fencePicketWhiteDoor_01_11.addComponentOrReplace(gltfShape_2)
const transform_13 = new Transform({
  position: new Vector3(15.5, 0, 13),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_11.addComponentOrReplace(transform_13)
engine.addEntity(fencePicketWhiteDoor_01_11)

const fencePicketWhiteDoor_01_12 = new Entity()
fencePicketWhiteDoor_01_12.setParent(scene)
fencePicketWhiteDoor_01_12.addComponentOrReplace(gltfShape_2)
const transform_14 = new Transform({
  position: new Vector3(15.5, 0, 14.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_12.addComponentOrReplace(transform_14)
engine.addEntity(fencePicketWhiteDoor_01_12)

const fencePicketWhiteDoor_01_13 = new Entity()
fencePicketWhiteDoor_01_13.setParent(scene)
fencePicketWhiteDoor_01_13.addComponentOrReplace(gltfShape_2)
const transform_15 = new Transform({
  position: new Vector3(15.5, 0, 15.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_13.addComponentOrReplace(transform_15)
engine.addEntity(fencePicketWhiteDoor_01_13)

const fencePicketWhiteDoor_01_14 = new Entity()
fencePicketWhiteDoor_01_14.setParent(scene)
fencePicketWhiteDoor_01_14.addComponentOrReplace(gltfShape_2)
const transform_16 = new Transform({
  position: new Vector3(12, 0, 15.5),
  rotation: new Quaternion(0, -0.7071067811865476, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_14.addComponentOrReplace(transform_16)
engine.addEntity(fencePicketWhiteDoor_01_14)

const fencePicketWhiteDoor_01_15 = new Entity()
fencePicketWhiteDoor_01_15.setParent(scene)
fencePicketWhiteDoor_01_15.addComponentOrReplace(gltfShape_2)
const transform_17 = new Transform({
  position: new Vector3(13.5, 0, 15.5),
  rotation: new Quaternion(0, -0.7071067811865476, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_15.addComponentOrReplace(transform_17)
engine.addEntity(fencePicketWhiteDoor_01_15)

const fencePicketWhiteDoor_01_16 = new Entity()
fencePicketWhiteDoor_01_16.setParent(scene)
fencePicketWhiteDoor_01_16.addComponentOrReplace(gltfShape_2)
const transform_18 = new Transform({
  position: new Vector3(9, 0, 15.5),
  rotation: new Quaternion(0, -0.7071067811865476, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_16.addComponentOrReplace(transform_18)
engine.addEntity(fencePicketWhiteDoor_01_16)

const fencePicketWhiteDoor_01_17 = new Entity()
fencePicketWhiteDoor_01_17.setParent(scene)
fencePicketWhiteDoor_01_17.addComponentOrReplace(gltfShape_2)
const transform_19 = new Transform({
  position: new Vector3(10.5, 0, 15.5),
  rotation: new Quaternion(0, -0.7071067811865476, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_17.addComponentOrReplace(transform_19)
engine.addEntity(fencePicketWhiteDoor_01_17)

const fencePicketWhiteDoor_01_18 = new Entity()
fencePicketWhiteDoor_01_18.setParent(scene)
fencePicketWhiteDoor_01_18.addComponentOrReplace(gltfShape_2)
const transform_20 = new Transform({
  position: new Vector3(6, 0, 15.5),
  rotation: new Quaternion(0, -0.7071067811865476, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_18.addComponentOrReplace(transform_20)
engine.addEntity(fencePicketWhiteDoor_01_18)

const fencePicketWhiteDoor_01_19 = new Entity()
fencePicketWhiteDoor_01_19.setParent(scene)
fencePicketWhiteDoor_01_19.addComponentOrReplace(gltfShape_2)
const transform_21 = new Transform({
  position: new Vector3(4.5, 0, 15.5),
  rotation: new Quaternion(0, -0.7071067811865476, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_19.addComponentOrReplace(transform_21)
engine.addEntity(fencePicketWhiteDoor_01_19)

const fencePicketWhiteDoor_01_20 = new Entity()
fencePicketWhiteDoor_01_20.setParent(scene)
fencePicketWhiteDoor_01_20.addComponentOrReplace(gltfShape_2)
const transform_22 = new Transform({
  position: new Vector3(2.658676548578372, 0, 15.5),
  rotation: new Quaternion(0, -0.7071067811865476, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_20.addComponentOrReplace(transform_22)
engine.addEntity(fencePicketWhiteDoor_01_20)

const fencePicketWhiteDoor_01_21 = new Entity()
fencePicketWhiteDoor_01_21.setParent(scene)
fencePicketWhiteDoor_01_21.addComponentOrReplace(gltfShape_2)
const transform_23 = new Transform({
  position: new Vector3(1, 0, 15.5),
  rotation: new Quaternion(0, -0.7071067811865476, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_21.addComponentOrReplace(transform_23)
engine.addEntity(fencePicketWhiteDoor_01_21)

const fencePicketWhiteDoor_01_22 = new Entity()
fencePicketWhiteDoor_01_22.setParent(scene)
fencePicketWhiteDoor_01_22.addComponentOrReplace(gltfShape_2)
const transform_24 = new Transform({
  position: new Vector3(0.5, 0, 13.5),
  rotation: new Quaternion(0, -1.0000000000000002, 0, 2.636779683484747e-16),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_22.addComponentOrReplace(transform_24)
engine.addEntity(fencePicketWhiteDoor_01_22)

const fencePicketWhiteDoor_01_23 = new Entity()
fencePicketWhiteDoor_01_23.setParent(scene)
fencePicketWhiteDoor_01_23.addComponentOrReplace(gltfShape_2)
const transform_25 = new Transform({
  position: new Vector3(0.5, 0, 12),
  rotation: new Quaternion(0, -1.0000000000000002, 0, 2.636779683484747e-16),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_23.addComponentOrReplace(transform_25)
engine.addEntity(fencePicketWhiteDoor_01_23)

const fencePicketWhiteDoor_01_24 = new Entity()
fencePicketWhiteDoor_01_24.setParent(scene)
fencePicketWhiteDoor_01_24.addComponentOrReplace(gltfShape_2)
const transform_26 = new Transform({
  position: new Vector3(0.5, 0, 10.5),
  rotation: new Quaternion(0, -1.0000000000000002, 0, 2.636779683484747e-16),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_24.addComponentOrReplace(transform_26)
engine.addEntity(fencePicketWhiteDoor_01_24)

const fencePicketWhiteDoor_01_25 = new Entity()
fencePicketWhiteDoor_01_25.setParent(scene)
fencePicketWhiteDoor_01_25.addComponentOrReplace(gltfShape_2)
const transform_27 = new Transform({
  position: new Vector3(0.5, 0, 9),
  rotation: new Quaternion(0, -1.0000000000000002, 0, 2.636779683484747e-16),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_25.addComponentOrReplace(transform_27)
engine.addEntity(fencePicketWhiteDoor_01_25)

const fencePicketWhiteDoor_01_26 = new Entity()
fencePicketWhiteDoor_01_26.setParent(scene)
fencePicketWhiteDoor_01_26.addComponentOrReplace(gltfShape_2)
const transform_28 = new Transform({
  position: new Vector3(0.5, 0, 7.5),
  rotation: new Quaternion(0, -1.0000000000000002, 0, 2.636779683484747e-16),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_26.addComponentOrReplace(transform_28)
engine.addEntity(fencePicketWhiteDoor_01_26)

const fencePicketWhiteDoor_01_27 = new Entity()
fencePicketWhiteDoor_01_27.setParent(scene)
fencePicketWhiteDoor_01_27.addComponentOrReplace(gltfShape_2)
const transform_29 = new Transform({
  position: new Vector3(0.5, 0, 4.5),
  rotation: new Quaternion(0, -1.0000000000000002, 0, 2.636779683484747e-16),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_27.addComponentOrReplace(transform_29)
engine.addEntity(fencePicketWhiteDoor_01_27)

const fencePicketWhiteDoor_01_28 = new Entity()
fencePicketWhiteDoor_01_28.setParent(scene)
fencePicketWhiteDoor_01_28.addComponentOrReplace(gltfShape_2)
const transform_30 = new Transform({
  position: new Vector3(0.46800643012547916, 0, 2.7029608080899),
  rotation: new Quaternion(0, -1.0000000000000002, 0, 2.636779683484747e-16),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_28.addComponentOrReplace(transform_30)
engine.addEntity(fencePicketWhiteDoor_01_28)

const fencePicketWhiteDoor_01_29 = new Entity()
fencePicketWhiteDoor_01_29.setParent(scene)
fencePicketWhiteDoor_01_29.addComponentOrReplace(gltfShape_2)
const transform_31 = new Transform({
  position: new Vector3(0.5, 0, 1),
  rotation: new Quaternion(0, -1.0000000000000002, 0, 2.636779683484747e-16),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_29.addComponentOrReplace(transform_31)
engine.addEntity(fencePicketWhiteDoor_01_29)

const fencePicketWhiteDoor_01_30 = new Entity()
fencePicketWhiteDoor_01_30.setParent(scene)
fencePicketWhiteDoor_01_30.addComponentOrReplace(gltfShape_2)
const transform_32 = new Transform({
  position: new Vector3(2.5, 0, 0.5),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_30.addComponentOrReplace(transform_32)
engine.addEntity(fencePicketWhiteDoor_01_30)

const fencePicketWhiteDoor_01_31 = new Entity()
fencePicketWhiteDoor_01_31.setParent(scene)
fencePicketWhiteDoor_01_31.addComponentOrReplace(gltfShape_2)
const transform_33 = new Transform({
  position: new Vector3(4, 0, 0.5),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_31.addComponentOrReplace(transform_33)
engine.addEntity(fencePicketWhiteDoor_01_31)

const fencePicketWhiteDoor_01_32 = new Entity()
fencePicketWhiteDoor_01_32.setParent(scene)
fencePicketWhiteDoor_01_32.addComponentOrReplace(gltfShape_2)
const transform_34 = new Transform({
  position: new Vector3(5.5, 0, 0.5),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_32.addComponentOrReplace(transform_34)
engine.addEntity(fencePicketWhiteDoor_01_32)

const fencePicketWhiteDoor_01_33 = new Entity()
fencePicketWhiteDoor_01_33.setParent(scene)
fencePicketWhiteDoor_01_33.addComponentOrReplace(gltfShape_2)
const transform_35 = new Transform({
  position: new Vector3(7, 0, 0.5),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_33.addComponentOrReplace(transform_35)
engine.addEntity(fencePicketWhiteDoor_01_33)

const fencePicketWhiteDoor_01_34 = new Entity()
fencePicketWhiteDoor_01_34.setParent(scene)
fencePicketWhiteDoor_01_34.addComponentOrReplace(gltfShape_2)
const transform_36 = new Transform({
  position: new Vector3(11, 0, 0.5),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_34.addComponentOrReplace(transform_36)
engine.addEntity(fencePicketWhiteDoor_01_34)

const fencePicketWhiteDoor_01_35 = new Entity()
fencePicketWhiteDoor_01_35.setParent(scene)
fencePicketWhiteDoor_01_35.addComponentOrReplace(gltfShape_2)
const transform_37 = new Transform({
  position: new Vector3(12.5, 0, 0.5),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_35.addComponentOrReplace(transform_37)
engine.addEntity(fencePicketWhiteDoor_01_35)

const fencePicketWhiteDoor_01_36 = new Entity()
fencePicketWhiteDoor_01_36.setParent(scene)
fencePicketWhiteDoor_01_36.addComponentOrReplace(gltfShape_2)
const transform_38 = new Transform({
  position: new Vector3(14, 0, 0.5),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_36.addComponentOrReplace(transform_38)
engine.addEntity(fencePicketWhiteDoor_01_36)

const fencePicketWhiteDoor_01_37 = new Entity()
fencePicketWhiteDoor_01_37.setParent(scene)
fencePicketWhiteDoor_01_37.addComponentOrReplace(gltfShape_2)
const transform_39 = new Transform({
  position: new Vector3(15.5, 0, 0.5),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865476),
  scale: new Vector3(1, 1, 1)
})
fencePicketWhiteDoor_01_37.addComponentOrReplace(transform_39)
engine.addEntity(fencePicketWhiteDoor_01_37)

const pond_01 = new Entity()
pond_01.setParent(scene)
const gltfShape_3 = new GLTFShape('models/Pond_01/Pond_01.glb')
pond_01.addComponentOrReplace(gltfShape_3)
const transform_40 = new Transform({
  position: new Vector3(3.5, 0, 11.5),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
pond_01.addComponentOrReplace(transform_40)
engine.addEntity(pond_01)



const point1 = new Vector3(5, 0, 10)
const point2 = new Vector3(10, 0, 5)
const point3 = new Vector3(6, 0, 6 )
const point4 = new Vector3(8, 0, 8)
const path: Vector3[] = [point1, point2, point3, point4]

const TURN_TIME = 0.9

@Component('lerpData')
export class LerpData {
  array: Vector3[] = path
  origin: number = 0
  target: number = 1
  fraction: number = 0
}

@Component("timeOut")
export class TimeOut {
  timeLeft: number
  constructor( time: number){
    this.timeLeft = time
  }
}

export const paused = engine.getComponentGroup(TimeOut)


const pegasusShape = new GLTFShape('models/pegasus/pegasus.gltf')
const pegasus = spawnGltfX(pegasusShape, 8,0,8,  1,1,1, 0.01,0.01,0.01)


export class PegasuskWalk {
  update(dt: number) {
    let transform = pegasus.getComponent(Transform)
    let path = pegasus.getComponent(LerpData)
    path.fraction += dt / 12
    if (path.fraction < 1) {
      transform.position = Vector3.Lerp(
        path.array[path.origin],
        path.array[path.target],
        path.fraction
      )
    } else {
      path.origin = path.target
      path.target += 1
      if (path.target >= path.array.length) {
        path.target = 0
      }
      path.fraction = 0
      transform.lookAt(path.array[path.target])
    }
  }
}


engine.addSystem(new PegasuskWalk())


const walkspeed = 1
const animator = new Animator();
let clipWalk = new AnimationState("Take 001_Armature_0")
//clipWalk.looping = true
clipWalk.setParams({speed: walkspeed, weight: 0.5})
animator.addClip(clipWalk);
pegasus.addComponent(animator);


pegasus.addComponent(new LerpData())

clipWalk.play()








// Create AudioClip object, holding audio file
const clipStep = new AudioClip('sounds/step.mp3')
const clipNitro = new AudioClip('sounds/nitro.mp3')

const sourceStep = new AudioSource(clipStep)
const sourceNitro = new AudioSource(clipNitro)

// Add AudioSource component to entity
pegasus.addComponent(sourceStep)

sourceStep.loop = true

// Play sound
sourceStep.playing = true
sourceStep.loop = true

// Add click interaction
pegasus.addComponent(new OnClick(e => {
  pegasus.addComponentOrReplace(sourceNitro)
  sourceNitro.playing = true
}))




/*
const hud:BuilderHUD =  new BuilderHUD()
hud.setDefaultParent(scene)
hud.attachToEntity(pegasus)
*/
